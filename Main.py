import discord
import pygsheets
import sys
import json
import re
from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive
import openpyxl
import asyncio
import time

config = None
print("Loading config.json")
with open('config.json') as f:
    config = json.load(f)
print("Loading google bot account...")
try:
    gc = pygsheets.authorize(client_secret="client_secrets.json")
except pygsheets.AuthenticationError as ex:
    print(f"Authentication error, exiting\n{ex}")
    sys.exit(1)
spreadsheet = None
print("Getting spreadsheet")
try:
    spreadsheet = gc.open_by_key(config["spreadsheetID"])
except pygsheets.SpreadsheetNotFound as ex:
    print(f"Spreadsheet not found or I don't have access to spreadsheet!\n{ex}")
    sys.exit(1)
print("Getting worksheet")
keysheet = None
try:
    keysheet = spreadsheet.worksheet_by_title(config["sheetName"])
except pygsheets.WorksheetNotFound as ex:
    print(f"Worksheet not found.\n{ex}")
    sys.exit(1)
print("Excel thingy loaded.")
print("Loading Google Drive API...")
gauth = GoogleAuth()
print("Authenticating...")
gauth.LoadCredentialsFile("credential.cred")
if gauth.credentials is None:
    # Authenticate if they're not there
    gauth.LocalWebserverAuth()
elif gauth.access_token_expired:
    # Refresh them if expired
    gauth.Refresh()
else:
    # Initialize the saved creds
    gauth.Authorize()
# Save the current credentials to a file
gauth.SaveCredentialsFile("credential.cred")
print("Initalizing Google Drive API...")
drive = GoogleDrive(gauth)
ExcelFile = None
ExcelWorksheet = None
class Functions():
    @staticmethod
    async def checkUserExists(username: str, userid: int):
        for i in config["usrCheckCols"]:
            for celltuple in ExcelWorksheet.iter_cols(min_col=i,max_col=i, values_only=False):
                for cell in celltuple:
                    if cell.value != None and (username in cell.value or str(userid) in cell.value):
                        print("User already got the key!")
                        return True
        return False

    @staticmethod
    async def getKey():
        for i in config["keyCols"]:
            for celltuple in ExcelWorksheet.iter_cols(min_col=i,max_col=i, values_only=False):
                for cell in celltuple:
                    if ExcelWorksheet.cell(cell.row,int(config["usrWriteCol"])).value == None and not config["excludeTextInKeyColumn"] in cell.value:
                        print("Found valid key.")
                        return cell
        return None

    @staticmethod
    async def getAvailableKeys():
        totalKey = 0
        totalAvailableKey = 0
        for i in config["keyCols"]:
            for celltuple in ExcelWorksheet.iter_cols(min_col=i,max_col=i, values_only=False):
                for cell in celltuple:
                    if not config["excludeTextInKeyColumn"] in cell.value:
                        totalKey += 1
                        if ExcelWorksheet.cell(cell.row,int(config["usrWriteCol"])).value == None:
                            totalAvailableKey += 1
        return totalKey, totalAvailableKey, (totalAvailableKey / totalKey) * 100
    
    @staticmethod
    def SynchorizeKeyDB():
        print("Downloading spreadsheet to local machine...")
        docsfile = drive.CreateFile({'id': config["spreadsheetID"]})
        print('Downloading file %s from Google Drive' % docsfile['title']) # 'hello.png'
        docsfile.GetContentFile('KeySheet.xlsx', mimetype='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
        print("Excel downloaded, begin load the Excel file.")
        global ExcelFile
        ExcelFile = openpyxl.load_workbook(filename = 'KeySheet.xlsx')
        global ExcelWorksheet
        ExcelWorksheet = ExcelFile[config["sheetName"]]
Functions.SynchorizeKeyDB()
# Very bad code, i know.

class keyClient(discord.Client):
    async def on_ready(self):
        await self.change_presence(activity=discord.Activity(type=discord.ActivityType.playing, name="Remade V3."))
        print("Key Butler remake launched.")

    async def on_message(self, message):
        prefix = 'k!'
        if message.author != self.user and message.author.bot == False:
            if prefix + 'count' == message.content.lower():
                async with message.channel.typing():
                    print("Triggered count command.")
                    Functions.SynchorizeKeyDB()
                    infoTuple = await Functions.getAvailableKeys()
                    embed = discord.Embed(title="Key Count",
                                        description=f"{infoTuple[1]} Steam keys left out of {infoTuple[0]}!\n"f"{infoTuple[2]}% of Steam keys remaining",
                                        color=0x1a58a6)
                    await message.channel.send(embed=embed)
            elif prefix + 'forcesync' == message.content.lower() and ((516636531436421123 in [y.id for y in message.author.roles]) or (message.guild.id == 687926932024590396)):
                print("Triggered force sync database")
                msgg = await message.channel.send("Force syncing database...")
                Functions.SynchorizeKeyDB()
                await msgg.edit(content="Synced database.")
            elif prefix + 'help' == message.content.lower():
                desc = None
                try:
                    desc = f"k!count - Get keys statics.\nType anything in {self.get_channel(668477530336133131).mention} (excluding the commands) to get a key."
                except:
                    desc = f"k!count - Get keys statics.\nType anything in key channel to get a key."
                embed = discord.Embed(title=f"Commands",description=desc,colour=discord.Color.green())
                await message.channel.send(embed=embed)
            else:
                channel = message.channel
                if channel.id != 713698343791886426 and channel.id != 668477530336133131:  # Test channel or the key channel.
                    return
                msgg = await channel.send("Checking user...")
                Functions.SynchorizeKeyDB()
                user = message.author
                user_name = user.name + "#" + user.discriminator
                print(f'Checking {user_name}')
                try:
                    if await Functions.checkUserExists(user_name, user.id):
                        await message.add_reaction("🔑")
                        await msgg.edit(content="You already have a key.")
                        return
                except pygsheets.RequestError:
                    await msgg.edit(content="Google is blocking the request, please try again later...")
                    return
                print("All check passed, probably new user.")
                await msgg.edit(content="Fetching key (this might take a minute)")
                keyCell = await Functions.getKey()
                if keyCell is not None:
                    print(f"Given key: {keyCell.value}")
                    msgstr = f"""Hi there!

Here is your steam key: {keyCell.value}

If you have any problems do not hesitate to ask.
We also have a active bugtracker where you can search for open issues.
For more information have a look at our wiki:

FAQ: https://kelteseth.gitlab.io/ScreenPlayDocs/faq/faq/
Open issues: https://gitlab.com/kelteseth/ScreenPlay/-/issues
Create Wallpaper: https://kelteseth.gitlab.io/ScreenPlayDocs/wallpaper/video_wallpaper/
You can vote for ScreenPlay on https://alternativeto.net/software/screenplay/"""
                    try:
                        msg = await user.send(content=msgstr, embed=None)
                        await msg.edit(content=msgstr, suppress=True)
                        await msgg.edit(content="Key sent, please check DM!")
                        await message.add_reaction("👌")
                    except discord.DiscordException as ex:
                        print(ex)
                        embed = discord.Embed(title="Error!",
                                            description="Failed to send key, it looks like you haven't enabled Allow direct message from server members yet!",
                                            colour=discord.Color.red())
                        embed.set_footer(text="Key Giver Bot coded by Deleted User while suffering from tests.\nRewrite V3 branch.")
                        await msgg.edit(content=user.mention,embed=embed)
                        return
                    else:
                        print("Key gave, begin write to database.")
                        usrrr = f"{str(user.id)} {user_name}"
                        print("Writing to local excel file...")
                        global ExcelWorksheet
                        ExcelWorksheet.cell(keyCell.row,int(config["usrWriteCol"])).value = usrrr
                        global ExcelFile
                        ExcelFile.save("KeySheet.xlsx")
                        print("Reloading excel file...")
                        ExcelFile = openpyxl.load_workbook(filename = 'KeySheet.xlsx')
                        ExcelWorksheet = ExcelFile[config["sheetName"]]
                        print("Writing to Google Docs file...")
                        keysheet.update_value((keyCell.row, config["usrWriteCol"]), usrrr)
                        print("OK")
                else:
                    await msgg.edit(content="There aren't any keys left, use k!count to check for available keys.")
                print("Done")
async def LoopSyncKey():
    while True:
        await asyncio.sleep(30)
        print("Begin automatic sync...")
        Functions.SynchorizeKeyDB()
        print("OK")
asyncio.ensure_future(LoopSyncKey())

kBot = keyClient()
kBot.run(config["botToken"])