FROM python:alpine3.8
COPY . /app
WORKDIR /app
# yarl needs gcc
RUN apk add build-base
RUN pip install -r requirements.txt
EXPOSE 5000
CMD python3 ./Main.py